﻿using System;
namespace org.hpcshelf.exception
{
    public class AbstractComponentNotFoundException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.ABSTRACT_COMPONENT_NOT_FOUND_EXCEPTION.ToString();

        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        private string library_path;

        public AbstractComponentNotFoundException(string library_path, string component_id, string system_id) : base(component_id, system_id, String.Format("The abstract component {0} does not exist in the catalog", library_path))
        {
            this.library_path = library_path;
        }

        public string LibraryPath { get { return library_path; } }
    }
}
