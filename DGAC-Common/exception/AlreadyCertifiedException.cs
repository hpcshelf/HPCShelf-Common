﻿using System;
namespace org.hpcshelf.exception
{
    public class AlreadyCertifiedException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.ALREADY_CERTIFIED_EXCEPTION.ToString();

        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public AlreadyCertifiedException(string component_id, string system_id) : base(component_id, system_id, String.Format("The component named {0} is certified in {1} system", component_id, system_id))
        {
        }

    }
}
