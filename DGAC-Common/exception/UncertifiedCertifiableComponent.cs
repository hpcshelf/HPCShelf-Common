﻿using System;
namespace org.hpcshelf.exception
{
    public class UncertifiedCertifiableException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.UNCERTIFIED_CERTIFIABLE_COMPONENT_EXCEPTION.ToString();

        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public UncertifiedCertifiableException(string component_id, string system_id) : base(component_id, system_id, String.Format("The certifiable component named {0} is not certified in {1} system", component_id, system_id))
        {
        }

    }
}
