﻿using System;
namespace org.hpcshelf.exception
{
    public class UnresolvedSystemPlatformException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.UNRESOLVED_SYSTEM_PLATFORM_EXCEPTION.ToString();
        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public UnresolvedSystemPlatformException(string platform_id, string system_id) : base(platform_id, system_id, String.Format("No platform was found for the contract of {0}", platform_id))
        {
        }
    }
}
