﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace org.hpcshelf.common.utils
{
    public class Utils
    {

        public static bool checkConnection(string url, int timeout, bool throwExceptions, out string errorMessage)
        {
            try
            {
                errorMessage = string.Empty;

                // try accessing the web service directly via it's URL
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Timeout = timeout;

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception("Error locating web service");
                }

                // try getting the WSDL?
                // asmx lets you put "?wsdl" to make sure the URL is a web service
                // could parse and validate WSDL here

            }
            catch (WebException ex)
            {
                // decompose 400- codes here if you like
                errorMessage = string.Format("Error testing connection to web service at" + " \"{0}\":\r\n{1}", url, ex);
                Trace.TraceError(errorMessage);
                if (throwExceptions)
                    throw new Exception(errorMessage, ex);
            }
            catch (Exception ex)
            {
                errorMessage = string.Format("Error testing connection to web service at " + "\"{0}\":\r\n{1}", url, ex);
                Trace.TraceError(errorMessage);
                if (throwExceptions)
                    throw new Exception(errorMessage, ex);
                return false;
            }

            return true;
        }

        public static string GetPublicIpAddress()
        {
            string publicIPAddress;

            try
            {
                var request = (HttpWebRequest)WebRequest.Create("http://ifconfig.me");

                request.UserAgent = "curl"; // this will tell the server to return the information as if the request was made by the linux "curl" command

                request.Method = "GET";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        publicIPAddress = reader.ReadToEnd();
                    }
                }
            }
            catch (System.Web.Services.Protocols.SoapException e)
            {
                Console.WriteLine("ERROR GetPublicIpAddress 1: {0}", e.Message);
                if (e.InnerException != null)
                    Console.WriteLine("ERROR GetPublicIpAddress(inner exception): {0}", e.InnerException.Message);

                publicIPAddress = "127.0.0.1";
            }
            catch (System.Net.WebException e)
            {
                Console.WriteLine("ERROR GetPublicIpAddress 2: {0}", e.Message);
                if (e.InnerException != null)
                    Console.WriteLine("ERROR GetPublicIpAddress (inner exception): {0}", e.InnerException.Message);

                publicIPAddress = "127.0.0.1";
            }

            return publicIPAddress.Replace("\n", "");
        }

    }
}